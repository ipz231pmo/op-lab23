#define _CRT_SECURE_NO_WARNINGS
// itoa doesnt work without define below
#define _CRT_NONSTDC_NO_DEPRECATE
#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <string.h>
void error(const char*);
struct Date {
	int day, month, year;
};
struct Address {
	char street[8], building[4], flat[4];
};
enum ParamNumber
{
	Name,
	LastName,
	Patronymic,
	Phone, 
	School,
	Class,
	Male,
	BirthDate,
	Height,
	Weight,
	homeAddress
};
struct SchoolBoy {
	char name[15], lastName[15], patronymic[15], phoneNumber[15], school[15], clas[5];
	bool male; 
	Date birthDate;
	double height, weight;
	Address homeAddress;
};

SchoolBoy* database = NULL;
int size;

int cmpName(const void* a, const void* b) {
	return strcmp( ((const SchoolBoy*)a)->name, ((const SchoolBoy*)b)->name);
}
int cmpLastName(const void* a, const void* b) {
	return strcmp(((const SchoolBoy*)a)->lastName, ((const SchoolBoy*)b)->lastName);
}
int cmpPatronymic(const void* a, const void* b) {
	return strcmp(((const SchoolBoy*)a)->patronymic, ((const SchoolBoy*)b)->patronymic);
}
int cmpPhoneNumber(const void* a, const void* b) {
	return strcmp(((const SchoolBoy*)a)->phoneNumber, ((const SchoolBoy*)b)->phoneNumber);
}
int cmpSchool(const void* a, const void* b) {
	return strcmp(((const SchoolBoy*)a)->school, ((const SchoolBoy*)b)->school);
}
int cmpClass(const void* a, const void* b) {
	return strcmp(((const SchoolBoy*)a)->clas, ((const SchoolBoy*)b)->clas);
}
int cmpHeight(const void* a, const void* b) {
	return ((const SchoolBoy*)a)->height - ((const SchoolBoy*)b)->height;
}
int cmpWeight(const void* a, const void* b) {
	return ((const SchoolBoy*)a)->weight - ((const SchoolBoy*)b)->weight;
}
int cmpGender(const void* a, const void* b) {
	return ((const SchoolBoy*)a)->male - ((const SchoolBoy*)b)->male;
}
int cmpAddress(const void* a, const void* b) {
	char tmpAddressA[100], tmpAddressB[100];

	strcpy(tmpAddressA, ((const SchoolBoy*)a)->homeAddress.street);
	strcat(tmpAddressA, " ");
	strcat(tmpAddressA, ((const SchoolBoy*)a)->homeAddress.building);
	strcat(tmpAddressA, " ");
	strcat(tmpAddressA, ((const SchoolBoy*)a)->homeAddress.flat);

	strcpy(tmpAddressB, ((const SchoolBoy*)b)->homeAddress.street);
	strcat(tmpAddressB, " ");
	strcat(tmpAddressB, ((const SchoolBoy*)b)->homeAddress.building);
	strcat(tmpAddressB, " ");
	strcat(tmpAddressB, ((const SchoolBoy*)b)->homeAddress.flat);

	return strcmp(tmpAddressA, tmpAddressB);
}
int cmpDate(const void* a, const void* b) {
	char tmpDateA[100], tmpDateB[100], tmp[100];

	strcpy(tmpDateA, itoa(((const SchoolBoy*)a)->birthDate.year, tmp, 10));
	strcat(tmpDateA, ".");
	strcat(tmpDateA, itoa(((const SchoolBoy*)a)->birthDate.month, tmp, 10));
	strcat(tmpDateA, ".");
	strcat(tmpDateA, itoa(((const SchoolBoy*)a)->birthDate.day, tmp, 10));

	strcpy(tmpDateB, itoa(((const SchoolBoy*)b)->birthDate.year, tmp, 10));
	strcat(tmpDateB, ".");
	strcat(tmpDateB, itoa(((const SchoolBoy*)b)->birthDate.month, tmp, 10));
	strcat(tmpDateB, ".");
	strcat(tmpDateB, itoa(((const SchoolBoy*)b)->birthDate.day, tmp, 10));

	return strcmp(tmpDateA, tmpDateB);
}
void Add(SchoolBoy val) {	
	database = (SchoolBoy*) realloc(database, sizeof(SchoolBoy)*(++size));
	database[size - 1] = val;
}
void Delete(int n) {
	if (size == 0) error("Deleting empty database");
	for (int i = n + 1; i < size; i++)
		database[i - 1] = database[i];
	database = (SchoolBoy*)realloc(database, sizeof(SchoolBoy) * (--size));
}
void Sort(ParamNumber param) {
	int (*fp)(const void*, const void*) = cmpName;
	switch (param)
	{
	case Name:
		fp = cmpName;
		break;
	case LastName:
		fp = cmpLastName;
		break;
	case Patronymic:
		fp = cmpPatronymic;
		break;
	case Phone:
		fp = cmpPhoneNumber;
		break;
	case School:
		fp = cmpSchool;
		break;
	case Class:
		fp = cmpClass;
		break;
	case Male:
		fp = cmpGender;
		break;
	case BirthDate:
		fp = cmpDate;
		break;
	case Height:
		fp = cmpHeight;
		break;
	case Weight:
		fp = cmpWeight;
		break;
	case homeAddress:
		fp = cmpAddress;
		break;
	default:
		error("Unknown sort param");
		break;
	}
	qsort(database, size, sizeof(SchoolBoy), fp);
}
int Find(ParamNumber paramName, const void* paramValue) {
	int (*fp)(const void*, const void*) = cmpName;
	SchoolBoy boy;
	switch (paramName){
	case Name:
		fp = cmpName;
		strcpy(boy.name, (const char*)paramValue);
		break;
	case LastName:
		fp = cmpLastName;
		strcpy(boy.lastName, (const char*)paramValue);
		break;
	case Patronymic:
		fp = cmpPatronymic;
		strcpy(boy.patronymic, (const char*)paramValue);
		break;
	case Phone:
		fp = cmpPhoneNumber;
		strcpy(boy.phoneNumber, (const char*)paramValue);
		break;
	case School:
		fp = cmpSchool;
		strcpy(boy.school, (const char*)paramValue);
		break;
	case Class:
		fp = cmpClass;
		strcpy(boy.clas, (const char*)paramValue);
		break;
	case Male:
		fp = cmpGender;
		boy.male = *(const bool*)(paramValue);
		break;
	case BirthDate:
		fp = cmpDate;
		boy.birthDate = *(const Date*)(paramValue);
		break;
	case Height:
		fp = cmpHeight;
		boy.height = *(const int*)(paramValue);
		break;
	case Weight:
		fp = cmpWeight;
		boy.weight = *(const int*)(paramValue);
		break;
	case homeAddress:
		fp = cmpAddress;
		strcpy(boy.homeAddress.building, ((const Address*)paramValue)->building);
		strcpy(boy.homeAddress.street, ((const Address*)paramValue)->street);
		strcpy(boy.homeAddress.flat, ((const Address*)paramValue)->flat);
		break;
	default:
		error("Unknown sort param");
		break;
	}
	for (int i = 0; i < size; i++) {
		if (fp(&boy, &database[i]) == 0) return i;
	}
	return -1;
}
void Print() {
	printf("Database:\n");
	printf("%-15s %-15s %-15s %-6s %-6s %-6s %-10s %-15s %-18s %-15s %-5s\n",
		"Name", "Last Name", "Patronymic", "Gender", "Height", "Weight", "Birth Date", "Number", "Address", "School", "Class");
	for (int i = 0; i < size; i++) {
		char tmpDate[100], tmpAddress[100], tmp[100];

		strcpy(tmpDate, itoa(database[i].birthDate.day, tmp, 10));
		strcat(tmpDate, ".");
		strcat(tmpDate, itoa(database[i].birthDate.month, tmp, 10));
		strcat(tmpDate, ".");
		strcat(tmpDate, itoa(database[i].birthDate.year, tmp, 10));

		strcpy(tmpAddress, database[i].homeAddress.street);
		strcat(tmpAddress, " ");
		strcat(tmpAddress, database[i].homeAddress.building);
		strcat(tmpAddress, " ");
		strcat(tmpAddress, database[i].homeAddress.flat);

		printf("%-15s %-15s %-15s %-6s %-6.0f %-6.0f %-10s %-15s %-18s %-15s %-5s\n", 
			database[i].name,
			database[i].lastName,
			database[i].patronymic,
			database[i].male ? "Male" : "Female",
			database[i].height,
			database[i].weight,
			tmpDate,
			database[i].phoneNumber,
			tmpAddress,
			database[i].school,
			database[i].clas
		);
	}
}
int main(){	
	Add({ "Ivan", "Ivanov", "Ivanovich",   "0964328336", "C Liceum", "11B", true, {31, 7, 2006}, 167, 55, {"Sah str", "32", "a"} });
	Add({ "Ira", "Melnik", "Stepanivna",   "0661482794", "A Liceum", "11D", false, {22, 7, 2006}, 169, 58, {"Sah str", "42", "b"} });
	Add({ "Olga", "Steshin", "Leonidivna", "0986531258", "A Liceum", "8A", false, {24, 6, 2006}, 157, 41, {"Dun str", "16", "b"} });
	Add({ "Ihor", "Antonov", "Dmytrovich", "0973651043", "B Liceum", "4C", true, {14, 3, 2005}, 177, 74, {"Ole str", "11", "c"} });
	Print();
	printf("Sorting by Birth Date...\n");
	Sort(BirthDate);
	Print();
	int h = 157;
	printf("Student with height 157 has %d number\n", Find(Height, &h) + 1);
	printf("Deleting first schoolboy...\n");
	Delete(0);
	Print();
	free(database);
}
void error(const char* errorCode) {
	printf(errorCode);
	exit(-1);
}